# Copyright 2008 Fernando J. Pereda
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A text-based email client"
HOMEPAGE="http://www.mutt.org/"

DOWNLOADS="ftp://ftp.mutt.org/pub/mutt/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~x86"
MYOPTIONS="autocrypt debug doc gpgme idn sasl
    (
        gdbm [[ description = [ use gdbm for header caching ] ]]
        tokyocabinet [[ description = [ use tokyocabinet for header caching ] ]]
    ) [[ number-selected = exactly-one ]]
    (
        ncurses [[ description = [ use ncurses for the user interface ] ]]
        slang [[ description = [ use slang for the user interface ] ]]
    ) [[ number-selected = exactly-one ]]
    debug [[ description = [ Add debugging support and -d switch ] ]]
    gpgme [[ description = [ Enable support for gpgme ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        sys-apps/texinfo
        sys-devel/gettext
        doc? (
            app-text/docbook-utils
            app-text/docbook-xml-dtd:4.2
        )
    build+run:
        autocrypt? ( dev-db/sqlite:3 )
        gdbm? ( sys-libs/gdbm )
        gpgme? ( app-crypt/gpgme[>=1.4.0] )
        idn? (
            dev-libs/libunistring
            net-dns/libidn2:=
        )
        ncurses? ( sys-libs/ncurses )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        sasl? ( net-libs/cyrus-sasl )
        slang? ( sys-libs/slang )
        tokyocabinet? ( dev-db/tokyocabinet )
    suggestion:
        net-www/elinks [[ description = [ Provides a text based browser for viewing HTML-formatted mail ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --with-ssl
    --sysconfdir=/etc/${PN}
    --with-docdir=/usr/share/doc/${PNV}
    --with-homespool=.maildir
    --without-bundled-regex
    --enable-sidebar

    --enable-flock
    --disable-fcntl
    --enable-nfs-fix
    --enable-external-dotlock

    --enable-filemonitor
    --enable-imap
    --enable-pop
    --enable-smtp

    # hcache
    --enable-hcache
    --without-bdb
    --without-qdbm

    --without-gnutls
    --without-idn
)

DEFAULT_SRC_CONFIGURE_OPTIONS=( 'ncurses --with-curses' 'slang --with-slang' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( autocrypt debug doc 'doc full-doc' gpgme )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'autocrypt sqlite3' gdbm sasl 'idn idn2' tokyocabinet )
