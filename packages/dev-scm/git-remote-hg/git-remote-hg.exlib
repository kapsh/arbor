# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mnauw tag=v${PV} ]

export_exlib_phases src_compile src_install

SUMMARY="Transparent bidirectional bridge between Git and Mercurial for Git"
DESCRIPTION="
git-remote-hg is the semi-official Mercurial bridge from Git project, once
installed, it allows you to clone, fetch and push to and from Mercurial
repositories as if they were Git ones, e. g.:
git clone hg::http://selenic.com/repo/hello
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/asciidoc
    run:
        dev-lang/python:=[<3.0.0]
        dev-scm/mercurial
         !dev-scm/git-remote-helpers[mercurial] [[
            description = [ A maintained fork of the one in git-remote-helpers ]
            resolution = uninstall-blocked-after
        ]]
"

git-remote-hg_src_compile() {
    default

    emake doc
}

git-remote-hg_src_install() {
    emake DESTDIR="${IMAGE}" prefix=/usr/$(exhost --target) install
    emake DESTDIR="${IMAGE}" prefix=/usr install-doc
}

