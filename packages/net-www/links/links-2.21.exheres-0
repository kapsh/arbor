# Copyright 2008 Santiago M. Mola
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'links-2.2.ebuild', which is:
#     Copyright 1999-2008 Gentoo Foundation

MY_PNV="${PNV/_/}"

SUMMARY="A fast lightweight text and graphic web-browser"
HOMEPAGE="http://links.twibright.com"
DOWNLOADS="${HOMEPAGE}/download/${MY_PNV}.tar.bz2"

UPSTREAM_CHANGELOG="http://links.twibright.com/download/ChangeLog"

LICENCES="GPL-2"
SLOT="2"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    X
    brotli [[ description = [ Brotli compression format support ] ]]
    fbcon [[ requires = gpm ]]
    freetype [[ description = [ Makes it possible to select fonts, using fontconfig and freetype ] ]]
    gpm
    jpeg
    openmp [[ description = [ Support for using Open Multi-Processing in the image scaler ] ]]
    tiff
    zstd [[ description = [ Support for the ZStandard compression algorithm ] ]]

    freetype? ( ( X fbcon ) [[ number-selected = at-least-one ]] )
    jpeg? ( ( X fbcon ) [[ number-selected = at-least-one ]] )
    openmp? ( ( X fbcon ) [[ number-selected = at-least-one ]] )
    tiff? ( ( X fbcon ) [[ number-selected = at-least-one ]] )

    jpeg? ( ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]] )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        app-arch/xz
        dev-libs/libbsd [[ note = [ automagic ] ]]
        sys-libs/ncurses
        sys-libs/zlib
        X? (
            media-libs/libpng:=[>=1.2.1]
            x11-libs/libX11
            jpeg? (
                providers:ijg-jpeg? ( media-libs/jpeg:= )
                providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
            )
            openmp? ( sys-libs/libgomp:= )
            tiff? ( media-libs/tiff[>=3.5.7] )
        )
        brotli? ( app-arch/brotli )
        fbcon? (
            media-libs/libpng:=[>=1.2.1]
            jpeg? (
                providers:ijg-jpeg? ( media-libs/jpeg:= )
                providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
            )
            openmp? ( sys-libs/libgomp:= )
            tiff? ( media-libs/tiff[>=3.5.7] )
        )
        freetype? (
            media-libs/fontconfig
            media-libs/freetype
        )
        gpm? ( sys-libs/gpm )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        zstd? ( app-arch/zstd )
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( BRAILLE_HOWTO KEYS SITES )

src_configure() {
    local myconf=(
        --hates=docdir
        --hates=datarootdir
        --disable-javascript
        --with-bzip2
        --with-ssl
        --with-zlib
        --without-directfb
        --without-libevent
        --without-librsvg
        --without-lzip
        --without-sdl
        --without-svgalib
        $(option_with X x)
        $(option_with brotli)
        $(option_with fbcon fb)
        $(option_with freetype)
        $(option_with gpm)
        $(option_with zstd)
    )

    if option X || option fbcon; then
        myconf+=(
            --enable-graphics
            $(option_with jpeg libjpeg)
            $(option_with openmp)
            $(option_with tiff libtiff)
        )
    else
        myconf+=( --disable-graphics )
    fi

    econf "${myconf[@]}"
}

src_install() {
    default

    # Only install links icon if X driver was compiled in
    if option X ; then
        insinto /usr/share/pixmaps
        doins graphics/links.xpm
    fi

    insinto /usr/share/doc/${PNVR}
    doins -r doc/links_cal

    # Install a compatibility symlink links2:
    dosym links /usr/$(exhost --target)/bin/links2
}

