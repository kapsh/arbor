Upstream: https://reviews.llvm.org/D70306
From 9045de80424764c0e245e2dcef2324cd1979352b Mon Sep 17 00:00:00 2001
From: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
Date: Fri, 15 Nov 2019 14:18:30 +0100
Subject: [PATCH 1/2] clang: Exherbo multiarch ajustments

Exherbo multiarch layout being somewhat specific,
some adjustments need to be made wrt the lookup
paths

Signed-off-by: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
---
 clang/lib/Driver/ToolChains/Linux.cpp        | 75 ++++++++++++++++++--
 clang/test/Driver/android-ndk-standalone.cpp |  4 +-
 clang/test/Driver/arm-multilibs.c            |  2 +
 clang/test/Driver/cross-linux.c              |  2 +
 clang/test/Driver/dyld-prefix.c              |  2 +
 clang/test/Driver/linux-header-search.cpp    |  2 +
 clang/test/Driver/linux-ld.c                 |  2 +
 clang/test/Driver/mips-cs.cpp                |  1 +
 clang/test/Driver/mips-fsf.cpp               |  1 +
 clang/test/Driver/mips-img.cpp               |  1 +
 clang/test/lit.cfg.py                        |  3 +
 13 files changed, 92 insertions(+), 7 deletions(-)

diff --git a/clang/lib/Driver/ToolChains/Linux.cpp b/clang/lib/Driver/ToolChains/Linux.cpp
index 087783875ff..93642486938 100644
--- a/clang/lib/Driver/ToolChains/Linux.cpp
+++ b/clang/lib/Driver/ToolChains/Linux.cpp
@@ -350,9 +350,21 @@ Linux::Linux(const Driver &D, const llvm::Triple &Triple, const ArgList &Args)
     //
     // Note that this matches the GCC behavior. See the below comment for where
     // Clang diverges from GCC's behavior.
-    addPathIfExists(D, LibPath + "/../" + GCCTriple.str() + "/lib/../" +
-                           OSLibDir + SelectedMultilib.osSuffix(),
-                    Paths);
+    //
+    // On Exherbo, the GCC installation will reside in e.g.
+    //   /usr/x86_64-pc-linux-gnu/lib/gcc/armv7-unknown-linux-gnueabihf/9.2.0
+    // while the matching lib path is
+    //   /usr/armv7-unknown-linux-gnueabihf/lib
+    if (Distro == Distro::Exherbo)
+      addPathIfExists(D,
+                      LibPath + "/../../" + GCCTriple.str() + "/lib/../" +
+                          OSLibDir + SelectedMultilib.osSuffix(),
+                      Paths);
+    else
+      addPathIfExists(D,
+                      LibPath + "/../" + GCCTriple.str() + "/lib/../" +
+                          OSLibDir + SelectedMultilib.osSuffix(),
+                      Paths);
 
     // If the GCC installation we found is inside of the sysroot, we want to
     // prefer libraries installed in the parent prefix of the GCC installation.
@@ -844,6 +856,39 @@ void Linux::AddClangSystemIncludeArgs(const ArgList &DriverArgs,
   if (getTriple().isAndroid())
     MultiarchIncludeDirs = AndroidMultiarchIncludeDirs;
 
+  // Exherbo's multiarch layout is /usr/<triple>/include and not
+  // /usr/include/<triple>
+  const Distro Distro(D.getVFS());
+  const StringRef ExherboMultiarchIncludeDirs[] = {"/usr/" + getTriple().str() +
+                                                   "/include"};
+  const StringRef ExherboX86_64MuslMultiarchIncludeDirs[] = {
+      "/usr/x86_64-pc-linux-musl/include"};
+  const StringRef ExherboX86_64MultiarchIncludeDirs[] = {
+      "/usr/x86_64-pc-linux-gnu/include"};
+  const StringRef ExherboI686MuslMultiarchIncludeDirs[] = {
+      "/usr/i686-pc-linux-musl/include"};
+  const StringRef ExherboI686MultiarchIncludeDirs[] = {
+      "/usr/i686-pc-linux-gnu/include"};
+  if (Distro == Distro::Exherbo) {
+    switch (getTriple().getArch()) {
+    case llvm::Triple::x86_64:
+      if (getTriple().isMusl())
+        MultiarchIncludeDirs = ExherboX86_64MuslMultiarchIncludeDirs;
+      else
+        MultiarchIncludeDirs = ExherboX86_64MultiarchIncludeDirs;
+      break;
+    case llvm::Triple::x86:
+      if (getTriple().isMusl())
+        MultiarchIncludeDirs = ExherboI686MuslMultiarchIncludeDirs;
+      else
+        MultiarchIncludeDirs = ExherboI686MultiarchIncludeDirs;
+      break;
+    default:
+      MultiarchIncludeDirs = ExherboMultiarchIncludeDirs;
+      break;
+    }
+  }
+
   for (StringRef Dir : MultiarchIncludeDirs) {
     if (D.getVFS().exists(SysRoot + Dir)) {
       addExternCSystemInclude(DriverArgs, CC1Args, SysRoot + Dir);
@@ -917,12 +962,30 @@ void Linux::addLibStdCxxIncludePaths(const llvm::opt::ArgList &DriverArgs,
   StringRef LibDir = GCCInstallation.getParentLibPath();
   StringRef InstallDir = GCCInstallation.getInstallPath();
   StringRef TripleStr = GCCInstallation.getTriple().str();
+  const Driver &D = getDriver();
   const Multilib &Multilib = GCCInstallation.getMultilib();
-  const std::string GCCMultiarchTriple = getMultiarchTriple(
-      getDriver(), GCCInstallation.getTriple(), getDriver().SysRoot);
+  const std::string GCCMultiarchTriple =
+      getMultiarchTriple(D, GCCInstallation.getTriple(), D.SysRoot);
   const std::string TargetMultiarchTriple =
-      getMultiarchTriple(getDriver(), getTriple(), getDriver().SysRoot);
+      getMultiarchTriple(D, getTriple(), D.SysRoot);
   const GCCVersion &Version = GCCInstallation.getVersion();
+  const Distro Distro(D.getVFS());
+
+  // On Exherbo the consecutive addLibStdCXXIncludePaths call would evaluate to:
+  //   LibDir = /usr/lib/gcc/<triple>/9.2.0/../../..
+  //          = /usr/lib/
+  //   LibDir + "/../include" = /usr/include
+  // addLibStdCXXIncludePaths would then check if "/usr/include/c++/<version>"
+  // exists, and add that as include path when what we want is
+  // "/usr/<triple>/include/c++/<version>"
+  // Note that "/../../" is needed and not just "/../" as /usr/include points to
+  // /usr/host/include
+  if (Distro == Distro::Exherbo &&
+      addLibStdCXXIncludePaths(
+          LibDir.str() + "/../../" + TripleStr + "/include",
+          "/c++/" + Version.Text, TripleStr, GCCMultiarchTriple,
+          TargetMultiarchTriple, Multilib.includeSuffix(), DriverArgs, CC1Args))
+    return;
 
   // The primary search for libstdc++ supports multiarch variants.
   if (addLibStdCXXIncludePaths(LibDir.str() + "/../include",
diff --git a/clang/test/Driver/android-ndk-standalone.cpp b/clang/test/Driver/android-ndk-standalone.cpp
index c4d93993478..7ce982ddb1c 100644
--- a/clang/test/Driver/android-ndk-standalone.cpp
+++ b/clang/test/Driver/android-ndk-standalone.cpp
@@ -1,6 +1,8 @@
+// XFAIL: exherbo
+
 // Test header and library paths when Clang is used with Android standalone
 // toolchain.
-//
+
 // RUN: %clang -no-canonical-prefixes %s -### -o %t.o 2>&1 \
 // RUN:     -target arm-linux-androideabi21 \
 // RUN:     -B%S/Inputs/basic_android_ndk_tree \
diff --git a/clang/test/Driver/arm-multilibs.c b/clang/test/Driver/arm-multilibs.c
index bd9c80e8b16..1de8739e7e9 100644
--- a/clang/test/Driver/arm-multilibs.c
+++ b/clang/test/Driver/arm-multilibs.c
@@ -1,3 +1,5 @@
+// XFAIL: exherbo
+
 // RUN: %clang -target armv7-linux-gnueabi --sysroot=%S/Inputs/multilib_arm_linux_tree -### -c %s -o /dev/null 2>&1 | FileCheck -check-prefix CHECK-ARM %s
 // RUN: %clang -target thumbv7-linux-gnueabi --sysroot=%S/Inputs/multilib_arm_linux_tree -### -c %s -o /dev/null 2>&1 | FileCheck -check-prefix CHECK-ARM %s
 
diff --git a/clang/test/Driver/cross-linux.c b/clang/test/Driver/cross-linux.c
index a5ea832e77e..5b95bada497 100644
--- a/clang/test/Driver/cross-linux.c
+++ b/clang/test/Driver/cross-linux.c
@@ -1,3 +1,5 @@
+// XFAIL: exherbo
+
 // RUN: %clang -### -o %t %s 2>&1 -no-integrated-as -fuse-ld=ld \
 // RUN:   --gcc-toolchain=%S/Inputs/basic_cross_linux_tree/usr \
 // RUN:   --target=i386-unknown-linux-gnu \
diff --git a/clang/test/Driver/dyld-prefix.c b/clang/test/Driver/dyld-prefix.c
index 5a79874b567..f20824ae18a 100644
--- a/clang/test/Driver/dyld-prefix.c
+++ b/clang/test/Driver/dyld-prefix.c
@@ -1,3 +1,5 @@
+// XFAIL: exherbo
+
 // RUN: touch %t.o
 
 // RUN: %clang -target i386-unknown-linux --dyld-prefix /foo -### %t.o 2>&1 | FileCheck --check-prefix=CHECK-32 %s
diff --git a/clang/test/Driver/linux-header-search.cpp b/clang/test/Driver/linux-header-search.cpp
index 03502423c15..f70ad00d03e 100644
--- a/clang/test/Driver/linux-header-search.cpp
+++ b/clang/test/Driver/linux-header-search.cpp
@@ -1,3 +1,5 @@
+// XFAIL: exherbo
+
 // General tests that the header search paths detected by the driver and passed
 // to CC1 are sane.
 //
diff --git a/clang/test/Driver/linux-ld.c b/clang/test/Driver/linux-ld.c
index 7aba660d51f..2fad14e1af3 100644
--- a/clang/test/Driver/linux-ld.c
+++ b/clang/test/Driver/linux-ld.c
@@ -1,3 +1,5 @@
+// XFAIL: exherbo
+
 // General tests that ld invocations on Linux targets sane. Note that we use
 // sysroot to make these tests independent of the host system.
 //
diff --git a/clang/test/Driver/mips-cs.cpp b/clang/test/Driver/mips-cs.cpp
index 6ef4c5d4350..7b2e89919f1 100644
--- a/clang/test/Driver/mips-cs.cpp
+++ b/clang/test/Driver/mips-cs.cpp
@@ -1,4 +1,5 @@
 // REQUIRES: mips-registered-target
+// XFAIL: exherbo
 //
 // Check frontend and linker invocations on Mentor Graphics MIPS toolchain.
 //
diff --git a/clang/test/Driver/mips-fsf.cpp b/clang/test/Driver/mips-fsf.cpp
index b94da697891..bb1082d8c96 100644
--- a/clang/test/Driver/mips-fsf.cpp
+++ b/clang/test/Driver/mips-fsf.cpp
@@ -1,4 +1,5 @@
 // REQUIRES: mips-registered-target
+// XFAIL: exherbo
 
 // Check frontend and linker invocations on FSF MIPS toolchain.
 //
diff --git a/clang/test/Driver/mips-img.cpp b/clang/test/Driver/mips-img.cpp
index c97bb9478e7..3a2bcaf0a12 100644
--- a/clang/test/Driver/mips-img.cpp
+++ b/clang/test/Driver/mips-img.cpp
@@ -1,4 +1,5 @@
 // REQUIRES: mips-registered-target
+// XFAIL: exherbo
 
 // Check frontend and linker invocations on the IMG MIPS toolchain.
 //
diff --git a/clang/test/lit.cfg.py b/clang/test/lit.cfg.py
index 1ffb6d094d7..dedf7b76a4d 100644
--- a/clang/test/lit.cfg.py
+++ b/clang/test/lit.cfg.py
@@ -193,3 +193,6 @@ if os.path.exists('/etc/gentoo-release'):
 
 if config.enable_shared:
     config.available_features.add("enable_shared")
+
+if os.path.exists('/etc/exherbo-release'):
+    config.available_features.add('exherbo')
-- 
2.24.0

