# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=htop-dev ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="An interactive process viewer for Linux"
HOMEPAGE="https://htop.dev/' ${HOMEPAGE}"

REMOTE_IDS="freshcode:${PN}"

UPSTREAM_CHANGELOG="https://github.com/htop-dev/${PN}/blob/master/ChangeLog"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    delay-accounting [[ description = [ Displays delay accounting info from the Linux kernel ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-libs/ncurses
        delay-accounting? ( net-libs/libnl:3.0 )
    suggestion:
        dev-util/strace  [[ description = [ Trace system calls of the selected process ] ]]
        sys-process/lsof [[ description = [ Display a list of open files of the selected process ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-configure.ac-axe-python-check.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # Help htop to find our ncurses library when cross-compiling
    # NOTE: this only works because ncursesw6-config is a bash script, ideally this would be done
    # using pkg-config, see https://github.com/hishamhm/htop/issues/671
    HTOP_NCURSESW_CONFIG_SCRIPT="/usr/$(exhost --target)/bin/ncursesw6-config"
    --enable-cgroup
    --enable-openvz
    --enable-taskstats
    --enable-unicode
    --enable-vserver
    --disable-ancient-vserver
    --disable-hwloc
    --disable-werror
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'delay-accounting delayacct' )

