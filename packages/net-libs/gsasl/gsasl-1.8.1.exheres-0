# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="The GNU SASL library"
HOMEPAGE="https://www.gnu.org/software/gsasl/"
DOWNLOADS="https://download.savannah.nongnu.org/releases/${PN}/lib${PNVR}.tar.gz"

LICENCES="LGPL-3 GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    idn
    (
        gcrypt [[ description = [ Use libgcrypt library as crypto backend ] ]]
        kernel [[ description = [ Use kernel as crypto backend ] ]]
        openssl [[ description = [ Use openssl library as crypto backend ] ]]
    ) [[ number-selected = exactly-one ]]
    openssl? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
    ( linguas: da de eo es fi fr ga hu id it nl pl pt_BR ro sk sr sv uk vi zh_CN )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.2]
        virtual/pkg-config[>=0.20]
    build+run:
        gcrypt? (
            dev-libs/libgcrypt[>=1.4.4]
            dev-libs/libgpg-error
        )
        idn? ( net-dns/libidn )
        openssl? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        )
"

WORK="${WORKBASE}/lib${PNV}"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-gssapi
    --disable-kerberos_v5
    --disable-ntlm
    --disable-static
    --disable-valgrind-tests
    --without-gssapi-impl
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'gcrypt libgcrypt'
    'idn stringprep'
    'kernel linux-crypto'
    openssl
)

src_test() {
    esandbox allow_net --bind "inet:127.0.0.1@80"
    esandbox allow_net --connect "inet:127.0.0.1@80"

    default

    esandbox disallow_net --connect "inet:127.0.0.1@80"
    esandbox disallow_net --bind "inet:127.0.0.1@80"
}

